const app = require('../../src/app');

describe('\'sky-events\' service', () => {
  it('registered the service', () => {
    const service = app.service('sky-events');
    expect(service).toBeTruthy();
  });
});
