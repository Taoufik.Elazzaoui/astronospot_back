/* eslint-disable no-console */

// spot-model.js - A KnexJS
//
// See http://knexjs.org/
// for more of what you can do here.
module.exports = function (app) {
  const db = app.get('knexClient');
  const tableName = 'spot';
  db.schema.hasTable(tableName).then(exists => {
    if(!exists) {
      db.schema.createTable(tableName, table => {
        table.increments('id');
        table.string('id_user');
        table.string('nom').notNull();
        table.string('longitude').notNull();
        table.string('latitude').notNull();
        table.text('description');
        table.text('photo');
        table.timestamp('created_at').defaultTo(db.fn.now());
      })
        .then(() => console.log(`Created ${tableName} table`))
        .catch(e => console.error(`Error creating ${tableName} table`, e));
    }
  });


  return db;
};
