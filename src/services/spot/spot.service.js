// Initializes the `spot` service on path `/spot`
const { Spot } = require('./spot.class');
const createModel = require('../../models/spot.model');
const hooks = require('./spot.hooks');

module.exports = function (app) {
  const options = {
    Model: createModel(app),
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/spot', new Spot(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('spot');

  service.hooks(hooks);
};
