const { Service } = require('feathers-knex');

exports.Spot = class Spot extends Service {
  constructor(options) {
    super({
      ...options,
      name: 'spot'
    });
  }
};
