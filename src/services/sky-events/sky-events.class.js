const { Service } = require('feathers-knex');

exports.SkyEvents = class SkyEvents extends Service {
  constructor(options) {
    super({
      ...options,
      name: 'sky_events'
    });
  }
};
