// Initializes the `sky-events` service on path `/sky-events`
const { SkyEvents } = require('./sky-events.class');
const createModel = require('../../models/sky-events.model');
const hooks = require('./sky-events.hooks');

module.exports = function (app) {
  const options = {
    Model: createModel(app),
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/sky-events', new SkyEvents(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('sky-events');

  service.hooks(hooks);
};
