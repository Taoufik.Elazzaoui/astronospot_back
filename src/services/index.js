const users = require('./users/users.service.js');
const spot = require('./spot/spot.service.js');
const skyEvents = require('./sky-events/sky-events.service.js');
const uploads = require('./uploads/uploads.service.js');
// eslint-disable-next-line no-unused-vars
module.exports = function (app) {
  app.configure(users);
  app.configure(spot);
  app.configure(skyEvents);
  app.configure(uploads);
};
